<?php

/**
 * @file
 * This file contains the page functions for media_sets module.
 */

/**
 * Form callback for creating a new media set.
 *
 * @param array $media
 *  An array containing file entities to be associated with the media set that's
 *  going to be created.
 */
function media_sets_create_set($form, &$form_state, $media = array()) {
  $form = array();

  $form['title'] = array(
    '#type' => 'textfield',
    '#title' => t('Title'),
    '#required' => TRUE,
  );

  $form['description'] = array(
    '#type' => 'textarea',
    '#title' => t('Description'),
  );

  $form['msid'] = array(
    '#type' => 'textfield',
    '#title' => t('MSID'),
  );

  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save'),
  );

  return $form;
}

/**
 * Submit callback for media_sets_create_set form.
 */
function media_sets_create_set_submit($form, &$form_state) {
  
  $media_set = new stdClass;
  $media_set->title = $form_state['values']['title'];
  $media_set->msid = $form_state['values']['msid'];
  $media_set->media = array(1, 3);

  media_set_save($media_set);

  drupal_set_message(t('Media Set created successfuly'), 'status');
}
